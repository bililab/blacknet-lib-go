package jsonrpc

import (
	"net/url"
)

// Serialize .
type Serialize struct {
	Endpoint string
	Request  *Request
}

// NewSerialize new NewSerialize
func NewSerialize(endpoint string) *Serialize {
	return &Serialize{
		Endpoint: endpoint,
		Request:  NewRequest(),
	}
}

// Transfer . url.Values{"username": {"auto"}, "password": {"auto123123"}}
func (s *Serialize) Transfer(data url.Values) *Body {
	return s.Request.PostForm(s.Endpoint+"/api/v2/serialize/transfer", data)
}

// Lease . url.Values{"username": {"auto"}, "password": {"auto123123"}}
func (s *Serialize) Lease(data url.Values) *Body {
	return s.Request.PostForm(s.Endpoint+"/api/v2/serialize/lease", data)
}

// CancelLease . url.Values{"username": {"auto"}, "password": {"auto123123"}}
func (s *Serialize) CancelLease(data url.Values) *Body {
	return s.Request.PostForm(s.Endpoint+"/api/v2/serialize/cancellease", data)
}
