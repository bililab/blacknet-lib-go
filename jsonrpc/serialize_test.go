package jsonrpc

import (
	"net/url"
	"strconv"
	"testing"
)

var serialize = NewSerialize("https://blnmobiledaemon.blnscan.io")

func Test_Blacknet_Serialize_Transfer(t *testing.T) {
	amount := int64(1e8)
	fee := int64(0.001 * 1e8)
	data := url.Values{"from": {testAccount2}, "to": {testAccount2}, "amount": {strconv.FormatInt(amount, 10)}, "fee": {strconv.FormatInt(fee, 10)}}
	data.Add("message", "blacknet")
	data.Add("encrypted", strconv.Itoa(1))
	res := serialize.Transfer(data)
	if res.Code != 200 {
		t.Errorf("Serialize.Transfer(%v) = %v; expected %v", data, res.Body, res.Code)
	}
}

func Test_Blacknet_Serialize_Lease(t *testing.T) {
	amount := int64(1e8)
	fee := int64(0.001 * 1e8)
	data := url.Values{"from": {testAccount2}, "to": {testAccount2}, "amount": {strconv.FormatInt(amount, 10)}, "fee": {strconv.FormatInt(fee, 10)}}
	res := serialize.Lease(data)
	if res.Code != 200 {
		t.Errorf("Serialize.Lease(%v) = %v; expected %v", data, res.Body, res.Code)
	}
}

func Test_Blacknet_Serialize_CancelLease(t *testing.T) {
	amount := int64(1e8)
	fee := int64(0.001 * 1e8)
	data := url.Values{"from": {testAccount2}, "to": {testAccount2}, "amount": {strconv.FormatInt(amount, 10)}, "fee": {strconv.FormatInt(fee, 10)}}
	data.Add("height", strconv.FormatInt(amount, 10))
	res := serialize.CancelLease(data)
	if res.Code != 200 {
		t.Errorf("Serialize.CancelLease(%v) = %v; expected %v", data, res.Body, res.Code)
	}
}
