package jsonrpc

import (
	"net/url"
	"strconv"
	"strings"

	blacknet "gitlab.com/bililab/blacknet-lib-go"
)

// Config .
type Config struct {
	Endpoint string
}

// JSONRPC .
type JSONRPC struct {
	Config    *Config
	Request   *Request
	Serialize *Serialize
}

// New jsonrpc
func New(config *Config) *JSONRPC {
	return &JSONRPC{
		Request:   NewRequest(),
		Serialize: NewSerialize(config.Endpoint),
		Config:    config,
	}
}

// Transfer .
func (s *JSONRPC) Transfer(mnemonic, from, to string, amount, fee int64, args ...interface{}) *Body {
	form := url.Values{"from": {from}, "to": {to}, "amount": {strconv.FormatInt(amount, 10)}, "fee": {strconv.FormatInt(fee, 10)}}
	msg, encrypted := formartArgs(args)
	if msg != "" {
		form.Add("message", msg)
	}
	if encrypted != -1 {
		form.Add("encrypted", strconv.Itoa(encrypted))
	}
	serializedRes := s.Serialize.Transfer(form)
	if serializedRes.Code != 200 {
		return serializedRes
	}
	signature := blacknet.Signature(mnemonic, serializedRes.Body)

	return s.Request.Get(strings.Join([]string{
		s.Config.Endpoint,
		"/api/v2/sendrawtransaction",
		signature,
	}, "/"))
}

// Lease .
func (s *JSONRPC) Lease(mnemonic, from, to string, amount, fee int64) *Body {
	form := url.Values{"from": {from}, "to": {to}, "amount": {strconv.FormatInt(amount, 10)}, "fee": {strconv.FormatInt(fee, 10)}}
	serializedRes := s.Serialize.Lease(form)
	if serializedRes.Code != 200 {
		return serializedRes
	}
	signature := blacknet.Signature(mnemonic, serializedRes.Body)
	return s.Request.Get(strings.Join([]string{
		s.Config.Endpoint,
		"/api/v2/sendrawtransaction",
		signature,
	}, "/"))
}

// CancelLease .
func (s *JSONRPC) CancelLease(mnemonic, from, to string, amount, fee, height int64) *Body {
	form := url.Values{"from": {from}, "to": {to}, "amount": {strconv.FormatInt(amount, 10)}, "height": {strconv.FormatInt(height, 10)}, "fee": {strconv.FormatInt(fee, 10)}}
	serializedRes := s.Serialize.CancelLease(form)
	if serializedRes.Code != 200 {
		return serializedRes
	}
	signature := blacknet.Signature(mnemonic, serializedRes.Body)
	return s.Request.Get(strings.Join([]string{
		s.Config.Endpoint,
		"/api/v2/sendrawtransaction",
		signature,
	}, "/"))
}
